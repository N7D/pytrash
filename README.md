# PyTrash #

This repository offers the web trash manager.

### What is this repository for? ###
	Ever wanted to have multiple trash bins storing your actions history and working via web browser?
	Here it is. With manager you can:
	
	* View status and parameters of all trash bins;
	* Browse through directories;
	* Delete directories & files by selection;
	* Delete directories & files via regex;
	* Recover deleted files;
	* Erase deleted files;
	* Inspect operations that are currently running;
	* View the history of recently completed operations;
	* Have the logs.

### Prerequisites ###
	* Python
	* Pip
	* Django

### How do I get set up? ###
	1. Сlone or download this repository;
	2. Run server via python django:
		* python manage.py runserver
	3. Create new tab in your browser by entering local IP:
		* http://127.0.0.1:8000/
	Visit help tab to get info about using manager.

### Who do I talk to? ###
	Aleksey Krylov
	Ya.AleksKryl@yandex.ru