"""lab3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from trashApp import views
from django.contrib import admin

urlpatterns = [
    url(r'^$',          views.trash_bin_list,        name="trash_bin_list"),
    url(r'^new/',       views.create_new_trash_bin,  name="create_new_trash_bin"),
    url(r'^inspect/',   views.inspect_trash_bin,     name="inspect_trash_bin"),
    url(r'^explorer/',  views.explorer,              name="explorer"),
    url(r'^tasks/',     views.tasks,                 name="tasks"),
    url(r'^history/',   views.history,               name="history"),
    url(r'^help/',      views.help_page,             name="help"),
    url(r'^about/',     views.about_page,            name="about"),
]
