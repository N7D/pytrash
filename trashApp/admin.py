# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import TrashBinsList, OperationsList, TasksList, DoneOperationsList, DoneTasksList
from django.contrib import admin

admin.site.register(TrashBinsList)
admin.site.register(OperationsList)
admin.site.register(TasksList)
admin.site.register(DoneOperationsList)
admin.site.register(DoneTasksList)
