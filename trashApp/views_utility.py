"""
Views utility module.

It contains function that build object for accessing core functionality and some small utility helpers.
"""

from __future__ import unicode_literals

import errno
import os
import random

from models import TasksList
from work import TrashBin


def check_box_to_bool_converter(data):
    """
    Takes string and checks if it is None or "on".

    Parameters:
        data - string
    Returns:
        True if the string is "on".
    """
    if data is None:
        return False
    elif data == "on":
        return True


def make_trash_bin_object(trash_bins_list_entry):
    """
    Takes trash bin entry and constructs object of trash bin library class.

    Parameters:
        trash_bins_list_entry - entry of trash bin from database model.
    Returns:
        Constructed trash bin object.
    """
    trash_bin = TrashBin.TrashBin(None)

    trash_bin.confirmation = False
    trash_bin.dry_run = trash_bins_list_entry.dry_run
    trash_bin.force_overwrite = trash_bins_list_entry.force_overwrite
    trash_bin.silent_mode = trash_bins_list_entry.silent_mode
    trash_bin.erase_by_time = trash_bins_list_entry.erase_by_time
    trash_bin.max_trashcan_capacity = trash_bins_list_entry.max_trashcan_capacity
    trash_bin.max_trashcan_size = trash_bins_list_entry.max_trashcan_size
    trash_bin.records_per_page = float("inf")
    trash_bin.time_until_auto_erase = trash_bins_list_entry.time_until_auto_erase
    trash_bin.path_to_trash = trash_bins_list_entry.path_to_trash

    trash_bin.trash_file_path = os.path.join(trash_bins_list_entry.path_to_trash, "files/")
    trash_bin.trash_info_path = os.path.join(trash_bins_list_entry.path_to_trash, "info/")
    trash_bin.trash_log_path = os.path.join(trash_bins_list_entry.path_to_trash, "logfile.log")

    trash_bin.BuildTrashCan()

    return trash_bin


def get_error_message(e):
    """
    Takes exception object and checks if it is ENOENT, EACCESS, EPERM or other.

    Parameters:
        e - exception object.
    Returns:
        string with error message.
    """
    if e.args[0] == errno.ENOENT:
        error_message = "File not found!"
    elif e.args[0] == errno.EACCES:
        error_message = "Permission denied!"
    elif e.args[0] == errno.EPERM:
        error_message = "Operation is not allowed!"
    else:
        error_message = e
    return error_message


def generate_task_name():
    tasks_list = TasksList.objects.all()

    while True:
        index = random.randint(0, 1000000)
        generated_name = "task#{}".format(index)
        name_not_occupied = True
        for task in tasks_list:
            if task.task_name == generated_name:
                name_not_occupied = False
                break
        if name_not_occupied:
            break
    return generated_name


def create_new_task(trash_bin, task_name, operation_name):
    """
    Takes parameters and creates new task and saves it to database.

    Parameters:
        trash_bin - entry of trash bin from database model.
        task_name - name of task from database model.
        operation_name - string with operation classification.
    Returns:
        string with error message.
    """
    new_task = TasksList(trash_bin=trash_bin, task_name=task_name, operation_name=operation_name)
    new_task.save()
    return new_task
