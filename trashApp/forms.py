from django import forms
from .models import *


class NewTrashForms(forms.ModelForm):
    class Meta:
        model = TrashBinsList
        exclude = ["active", "selected", "silent_mode"]
