import argparse


def ParseArgument():
    pars = argparse.ArgumentParser()

    pars.add_argument("-r",  "--recover", help="Recover files by name",                     action="store", nargs='+', default=[])
    pars.add_argument("-e",  "--erase",   help="Erase particular files from trash by name", action="store", nargs='+', default=[])
    pars.add_argument("-ee", "--empty",   help="Erases everything from trash",              action="store_true")
    pars.add_argument("-d",  "--delete",  help="Delete files by name",                      action="store", nargs='+', default=[])
    pars.add_argument("-dr", "--regular", help="Delete files by regex",                     action="store", type=str)
    pars.add_argument("-i",  "--inspect", help="Inspect every file in the trash",           action="store_true")

    pars.add_argument("-pcp", "--config_path",            help="Print path to config location",    action="store_true")
    pars.add_argument("-s",   "--save_arguments",         help="Save passed arguments to config",  action="store_true")

    pars.add_argument("-enc", "--enable_confirmation",    help="Toggle confirmation",              action="store", type=str)
    pars.add_argument("-edr", "--enable_dry_run",         help="Toggle dry-run",                   action="store", type=str)
    pars.add_argument("-efo", "--enable_force_overwrite", help="Toggle force overwrite",           action="store", type=str)
    pars.add_argument("-esm", "--enable_silent_mode",     help="Toggle silent mode",               action="store", type=str)
    pars.add_argument("-eet", "--enable_erase_by_time",   help="Toggle erasing by time",           action="store", type=str)
    pars.add_argument("-mtc", "--max_trashcan_capacity",  help="Set trashcan capacity",            action="store", type=int)
    pars.add_argument("-mts", "--max_trashcan_size",      help="Set trashcan size",                action="store", type=int)
    pars.add_argument("-rpp", "--records_per_page",       help="Set amount of records per page",   action="store", type=int)
    pars.add_argument("-tae", "--time_until_auto_erase",  help="Set time until auto-erase occurs", action="store", type=int)
    pars.add_argument("-pth", "--path_to_trash_bin",      help="Set abs path to desired location", action="store", type=str)

    pars.add_argument("-v",   "--version",                help="Version 1.0.4",                    action="store_true")
    return pars.parse_args()


def CheckForMultipleRequests(args):
    requests_count = 0
    received_arguments = args

    if received_arguments.recover:
        requests_count += 1
    if received_arguments.erase:
        requests_count += 1
    if received_arguments.empty:
        requests_count += 1
    if received_arguments.delete:
        requests_count += 1
    if received_arguments.regular:
        requests_count += 1
    if requests_count > 1:
        return True
    elif requests_count == 1:
        return False
    elif received_arguments.inspect or received_arguments.config_path or received_arguments.save_arguments:
        return False
    else:
        return None
