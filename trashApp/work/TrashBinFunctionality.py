import errno
from datetime import datetime

from Miscellaneous import *
from Utility import *


def DeleteFileByPath(file_path, trash_file_path, trash_info_path, path_to_trash, dry_run, silent_mode):
    logging.debug("- Trying to delete \"{}\":".format(file_path))
    if ((file_path + '/' == trash_file_path) or
            (file_path + '/' == path_to_trash) or
            (file_path + '/' == trash_info_path) or
            (file_path == trash_file_path) or
            (file_path == trash_info_path) or
            (file_path == path_to_trash)):
        SilentPrint(" Removing trashcan is prohibited!",
                    " Attempted to remove trashcan;", silent_mode)
        raise OSError(1)
    file_name = os.path.basename(file_path)
    try:
        file_name = CheckForUniqueness(file_name, trash_file_path)
        Move(file_name, file_path, trash_file_path, trash_file_path, dry_run)
        if not dry_run:
            with open("{}.trashinfo".format(trash_info_path + file_name), 'w') as trash_info_file:
                trash_info_file.writelines(["[Trash Info]\n", "{}\n".format(file_path), str(datetime.now())])

        SilentPrint(" \"{}\" deleted successfully".format(file_name),
                    " \"{}\" deleted successfully;".format(file_path), silent_mode)
    except OSError as e:
        if e.args[0] == errno.EACCES:
            SilentPrint(" Denied. \"{}\" is system file or directory".format(os.path.basename(file_path)),
                        " Attempted to delete system file or directory;", silent_mode)
            raise
        SilentPrint(" \"{}\" not found".format(os.path.basename(file_path)),
                    " Deleting of \"{}\" has failed;".format(file_path), silent_mode)
        raise


def EraseFileFromBin(file_name, trash_file_path, trash_info_path, dry_run, silent_mode):
    logging.debug("- Trying to erase \"{}\" of trash:".format(file_name))
    try:
        Erase(trash_file_path + file_name, dry_run)
        if not dry_run:
            os.remove("{}.trashinfo".format(trash_info_path + file_name))
        SilentPrint(" \"{}\" erased successfully".format(file_name),
                    " \"{}\" erased successfully;".format(file_name), silent_mode)
    except OSError as e:
        if e.args[0] == errno.EACCES:
            SilentPrint(" Denied. \"{}\" is system file or directory".format(file_name),
                        " Attempted to erase system file or directory;", silent_mode)
            raise
        SilentPrint(" \"{}\" not found".format(file_name),
                    " Erasing \"{}\" of trash has failed (file not found);".format(file_name), silent_mode)
        raise


def EraseEverythingInBin(trash_file_path, trash_info_path, dry_run, silent_mode):
    total_operations_performed = 0
    all_trash_files = os.listdir(trash_file_path)
    for file_name in all_trash_files:
        EraseFileFromBin(file_name, trash_file_path, trash_info_path, dry_run, silent_mode)
        total_operations_performed += 1
    return total_operations_performed


def RecoverFileFromBin(file_name, trash_file_path, trash_info_path, force_overwrite, dry_run, silent_mode):
    read_line_holder = None
    logging.debug("- Trying to recover \"{}\":".format(file_name))
    try:
        with open("{}.trashinfo".format(trash_info_path + file_name), 'r') as info_file:
            read_line_holder = info_file.read().splitlines()
    except IOError as e:
        if e.args[0] == errno.ENOENT:
            SilentPrint(" \"{}\" not found".format(file_name),
                        " \"{}.trashinfo\" not found in trash;".format(file_name), silent_mode)
            raise OSError(2)
    new_path = os.path.dirname(read_line_holder[1])
    origin_name = os.path.basename(read_line_holder[1])
    if not os.path.exists(new_path):
        os.makedirs(new_path)
    if force_overwrite:
        if os.path.exists(read_line_holder[1]):
            Erase(read_line_holder[1], dry_run)
    try:
        Move(origin_name, trash_file_path + file_name, trash_file_path, new_path, dry_run)
        if not dry_run:
            os.remove("{}.trashinfo".format(trash_info_path + file_name))
        SilentPrint(" \"{}\" recovered successfully".format(origin_name),
                    " \"{}\" recovered successfully;".format(file_name), silent_mode)
    except OSError as e:
        if e.args[0] == errno.EACCES:
            SilentPrint(" Denied. \"{}\" is system file or directory".format(file_name),
                        " Attempted to recover system file or directory;", silent_mode)
            raise
        elif e.args[0] == errno.ENOENT:
            SilentPrint(" File not found",
                        " Recovery of \"{}\" has failed;".format(file_name), silent_mode)
            raise


def InspectBin(trash_file_path, records_per_page):

    index = 0
    file_list = []
    for file_name in os.listdir(trash_file_path):

        file_list.append(file_name)
        print (" - \"" + file_name + "\" ")
        index += 1
        if index == records_per_page and len(os.listdir(trash_file_path)) > records_per_page:
            index = 0
            ask_next = raw_input(" Show next " + str(records_per_page) + " records?(Y/N): ")
            if ask_next == 'y' or ask_next == 'Y':
                continue
            else:
                break
    return file_list