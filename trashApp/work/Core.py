from work import ArgParser
from work import TrashBin
from work.TrashBinConfig import *
from work.Statistics import *


def coreFunction():
    received_arguments = ArgParser.ParseArgument()
    trash_bin = TrashBin.TrashBin(BinConfig(received_arguments))
    stat = Statictics()

    if ArgParser.CheckForMultipleRequests(received_arguments) is None:
        print " Too few arguments!"
        return
    if ArgParser.CheckForMultipleRequests(received_arguments):
        print " Multiple requests are not allowed!"
        return
    trash_bin.AutoEraseCheck()
########################################################################################################################
    if received_arguments.recover:
        for file_name in received_arguments.recover:
            try:
                trash_bin.Recover(file_name)
            except OSError as e:
                stat.CountException(e.args[0])
        if not trash_bin.silent_mode:
            print(" - {} of {} files recovered\n - {} files failed"
                  .format(len(received_arguments.recover) - stat.total_amount_of_files,
                          len(received_arguments.recover), stat.not_found_files_amount + stat.system_files_amount))
########################################################################################################################
    if received_arguments.erase:
        for file_name in received_arguments.erase:
            try:
                trash_bin.Erase(file_name)
            except OSError as e:
                stat.CountException(e.args[0])
            if not trash_bin.silent_mode:
                print(" - {} of {} files erased\n - {} files failed"
                      .format(len(received_arguments.erase) - stat.total_amount_of_files,
                              len(received_arguments.erase), stat.not_found_files_amount + stat.system_files_amount))
########################################################################################################################
    if received_arguments.empty:
        total = 0
        try:
            total = trash_bin.Empty()
        except OSError as e:
            stat.CountException(e.args[0])
        if not trash_bin.silent_mode:
            print(" - {} of {} files erased\n - {} files failed"
                  .format(total - stat.total_amount_of_files,
                          total, stat.system_files_amount))
########################################################################################################################
    if received_arguments.delete:
        for file_name in received_arguments.delete:
            file_path = os.path.join(os.getcwd(), file_name)
            try:
                trash_bin.Delete(file_path)
            except OSError as e:
                stat.CountException(e.args[0])
        if not trash_bin.silent_mode:
            print(" - {} of {} files deleted\n - {} files failed"
                  .format(len(received_arguments.delete) - stat.total_amount_of_files,
                          len(received_arguments.delete),
                          stat.not_found_files_amount + stat.system_files_amount + stat.not_permitted_files_amount))

########################################################################################################################
    if received_arguments.regular:
        path_for_regular = os.getcwd()
        total = trash_bin.DeleteRecursive(path_for_regular, received_arguments.regular, stat)
        if not trash_bin.silent_mode:
            print(" - {} of {} files deleted\n - {} files failed"
                  .format(total - stat.total_amount_of_files,
                          total,
                          stat.not_found_files_amount + stat.system_files_amount + stat.not_permitted_files_amount))
########################################################################################################################
    if received_arguments.inspect:
        trash_bin.Inspect()
