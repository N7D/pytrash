import errno


class Statictics(object):
    def __init__(self):
        self.total_amount_of_files = 0
        self.not_found_files_amount = 0
        self.system_files_amount = 0
        self.not_permitted_files_amount = 0

    def CountException(self, argument):
        if argument == errno.ENOENT:
            self.not_found_files_amount += 1
        elif argument == errno.EACCES:
            self.system_files_amount += 1
        elif argument == errno.EPERM:
            self.not_permitted_files_amount += 1
        self.total_amount_of_files += 1
