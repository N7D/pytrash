import json
import os
import errno
from os.path import dirname, abspath
from Miscellaneous import *


class BinConfig(object):
    def __init__(self, args):
        if args is None:
            return
        self.trash_config_path = os.path.join(dirname(dirname(abspath(__file__))), "config.json")
        config = self.LoadConfigFile(args)
        try:
            self.path_to_trash = config["path_to_trash"]
            self.trash_file_path = os.path.join(config["path_to_trash"], "files/")
            self.trash_info_path = os.path.join(config["path_to_trash"], "info/")
            self.trash_log_path = os.path.join(config["path_to_trash"], "logfile.log")

            self.confirmation = None
            self.dry_run = None
            self.force_overwrite = None
            self.silent_mode = None
            self.erase_by_time = None
            self.max_trashcan_capacity = None
            self.max_trashcan_size = None
            self.records_per_page = None

            if type(config["time_until_auto_erase"]) is not int:
                self.time_until_auto_erase = int(config["time_until_auto_erase"])
            else:
                self.time_until_auto_erase = config["time_until_auto_erase"]

            logging.basicConfig(filename=self.trash_log_path, level=logging.DEBUG,
                                format='[%(asctime)s] -%(message)s')
            logging.debug(" ")
            if StrToBool(config["enable_dry_run"]):
                logging.debug(" <executing in dry_run>")
            logging.debug("-- Trying to load all arguments: ")

            self.LoadArguments(config)

            logging.debug(" Arguments loaded successfully;")

            if args.config_path:
                print (" Follow this path to locate a config:\n- " + self.trash_config_path)

        except ValueError:
            logging.ERROR(" Arguments load failed;")
            print " Bad arguments!"
            return
        except IOError as e:
            if e.args[0] == errno.ENOENT:
                print " Error: the path to trash bin is broken! Resetting to path by default."
                if os.path.exists(self.trash_config_path):
                    with open(self.trash_config_path, 'rw') as cfg:
                        config_reset = json.load(cfg)
                        config_reset["path_to_trash"] = self.LoadConfigByDefault()["path_to_trash"]
                        self.SaveConfig(self.trash_config_path, config_reset)
                self.__init__(args)
                return
            raise

    def LoadArguments(self, config):
        self.path_to_trash = config["path_to_trash"]
        self.trash_file_path = os.path.join(config["path_to_trash"], "files/")
        self.trash_info_path = os.path.join(config["path_to_trash"], "info/")
        self.trash_log_path = os.path.join(config["path_to_trash"], "logfile.log")
        if type(config["enable_confirmation"]) is not bool:
            self.confirmation = StrToBool(config["enable_confirmation"])
        else:
            self.confirmation = config["enable_confirmation"]

        if type(config["enable_dry_run"]) is not bool:
            self.dry_run = StrToBool(config["enable_dry_run"])
        else:
            self.dry_run = config["enable_dry_run"]

        if type(config["enable_force_overwrite"]) is not bool:
            self.force_overwrite = StrToBool(config["enable_force_overwrite"])
        else:
            self.force_overwrite = config["enable_force_overwrite"]

        if type(config["enable_silent_mode"]) is not bool:
            self.silent_mode = StrToBool(config["enable_silent_mode"])
        else:
            self.silent_mode = config["enable_silent_mode"]

        if type(config["enable_erase_by_time"]) is not bool:
            self.erase_by_time = StrToBool(config["enable_erase_by_time"])
        else:
            self.erase_by_time = config["enable_erase_by_time"]

        if type(config["max_trashcan_capacity"]) is not int:
            self.max_trashcan_capacity = int(config["max_trashcan_capacity"])
        else:
            self.max_trashcan_capacity = config["max_trashcan_capacity"]

        if type(config["max_trashcan_size"]) is not int:
            self.max_trashcan_size = int(config["max_trashcan_size"])
        else:
            self.max_trashcan_size = config["max_trashcan_size"]

        if type(config["records_per_page"]) is not int:
            self.records_per_page = int(config["records_per_page"])
        else:
            self.records_per_page = config["records_per_page"]

        if type(config["time_until_auto_erase"]) is not int:
            self.time_until_auto_erase = int(config["time_until_auto_erase"])
        else:
            self.time_until_auto_erase = config["time_until_auto_erase"]

    def LoadConfigFile(self, args):
        if os.path.exists(self.trash_config_path):
            with open(self.trash_config_path, 'rw') as cfg:
                config = json.load(cfg)
        else:
            config = self.LoadConfigByDefault()
            self.SaveConfig(self.trash_config_path, config)
        with open(os.path.expanduser('~') + "/human_config", 'a+') as cfg:
            lines = cfg.read().splitlines()
            for line in lines:
                key_value = line.split(' ')
                config[key_value[0]] = key_value[1]
        config = self.GetUserArgs(config, args)
        if args.save_arguments:
            self.SaveConfig(self.trash_config_path, config)
        return config

    @staticmethod
    def LoadConfigByDefault():

        config = {"path_to_trash": "/home/n7d/Desktop/trash/",
                  "enable_confirmation": True,
                  "enable_dry_run": False,
                  "enable_silent_mode": False,
                  "enable_force_overwrite": False,
                  "enable_erase_by_time": False,
                  "records_per_page": 10,
                  "time_until_auto_erase": 30000,
                  "max_trashcan_capacity": 6,
                  "max_trashcan_size": 50000
                  }

        return config

    @staticmethod
    def SaveConfig(trash_config_path, config):

        with open(trash_config_path, 'w') as cfg:
            json.dump(config, cfg)

    @staticmethod
    def GetUserArgs(cfg, arg):

        if arg.enable_confirmation:
            cfg["enable_confirmation"]    = arg.enable_confirmation
        if arg.enable_dry_run:
            cfg["enable_dry_run"]         = arg.enable_dry_run
        if arg.enable_silent_mode:
            cfg["enable_silent_mode"]     = arg.enable_silent_mode
        if arg.enable_force_overwrite:
            cfg["enable_force_overwrite"] = arg.enable_force_overwrite
        if arg.enable_erase_by_time:
            cfg["enable_erase_by_time"]   = arg.enable_erase_by_time
        if arg.records_per_page:
            cfg["records_per_page"]       = arg.records_per_page
        if arg.time_until_auto_erase:
            cfg["time_until_auto_erase"]  = arg.time_until_auto_erase
        if arg.max_trashcan_capacity:
            cfg["max_trashcan_capacity"]  = arg.max_trashcan_capacity
        if arg.max_trashcan_size:
            cfg["max_trashcan_size"] = arg.max_trashcan_size
        if arg.path_to_trash_bin:
            if os.path.isabs(arg.path_to_trash_bin) and os.path.exists(arg.path_to_trash_bin):
                cfg["path_to_trash"]  = arg.path_to_trash_bin
            else:
                print (" Given path to config is invalid or not existing, try again.\n")
        return cfg
