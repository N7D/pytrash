import re

from datetime import datetime
import TrashBinFunctionality
from TrashBinConfig import *
from Utility import *


class TrashBin(object):
    def __init__(self, config):
        if config is None:
            return
        self.confirmation = config.confirmation
        self.dry_run = config.dry_run
        self.force_overwrite = config.force_overwrite
        self.silent_mode = config.silent_mode
        self.erase_by_time = config.erase_by_time
        self.max_trashcan_capacity = config.max_trashcan_capacity
        self.max_trashcan_size = config.max_trashcan_size
        self.records_per_page = config.records_per_page
        self.time_until_auto_erase = config.time_until_auto_erase
        self.path_to_trash = config.path_to_trash
        self.trash_file_path = config.trash_file_path
        self.trash_info_path = config.trash_info_path

        self.BuildTrashCan()

    def BuildTrashCan(self):
        if os.path.exists(self.path_to_trash):
            if not os.path.exists(self.trash_file_path):
                os.makedirs(self.trash_file_path)
            if not os.path.exists(self.trash_info_path):
                os.makedirs(self.trash_info_path)
        else:
            SilentPrint(" ",
                        " Building trash bin;", self.silent_mode)
            os.makedirs(self.trash_file_path)
            os.makedirs(self.trash_info_path)

    def AutoEraseCheck(self):
        if self.erase_by_time:
            self.AutoErase()

########################################################################################################################
    def Recover(self, file_name):
        if not os.path.exists(self.trash_file_path + file_name):
            SilentPrint(" File \"{}\" not found".format(file_name),
                        " File \"{}\" not found;".format(file_name), self.silent_mode)
            raise OSError(2)
        if self.confirmation and not self.silent_mode:
            question = raw_input(" Do you really want to recover \"{}\"?(Y/N): ".format(file_name))
            if question == 'y' or question == 'Y':
                TrashBinFunctionality.RecoverFileFromBin(file_name, self.trash_file_path, self.trash_info_path,
                                                         self.force_overwrite,
                                                         self.dry_run,
                                                         self.silent_mode)
        else:
            TrashBinFunctionality.RecoverFileFromBin(file_name, self.trash_file_path, self.trash_info_path,
                                                     self.force_overwrite,
                                                     self.dry_run,
                                                     self.silent_mode)

    def Erase(self, file_name):

        if not os.path.exists(self.trash_file_path + file_name):
            SilentPrint(" File \"{}\" not found".format(file_name),
                        " File \"{}\" not found;".format(file_name), self.silent_mode)
            raise OSError(2)
        if self.confirmation and not self.silent_mode:
            question = raw_input(" Do you really want to erase file?(Y/N): ")
            if question == 'y' or question == 'Y':
                TrashBinFunctionality.EraseFileFromBin(file_name, self.trash_file_path,
                                                       self.trash_info_path, self.dry_run, self.silent_mode)
        else:
            TrashBinFunctionality.EraseFileFromBin(file_name, self.trash_file_path, self.trash_info_path, self.dry_run,
                                                   self.silent_mode)

    def Empty(self):
        logging.debug("- Trying to empty the bin:")
        total = 0
        if len(os.listdir(self.trash_info_path)) == 0:
            SilentPrint(" Trash bin is already empty", " Already empty;", self.silent_mode)
            return total
        if self.confirmation and not self.silent_mode:
            question = raw_input(" Do you really want to erase everything?(Y/N): ")
            if question == 'y' or question == 'Y':
                total = TrashBinFunctionality.EraseEverythingInBin(self.trash_file_path, self.trash_info_path,
                                                                   self.dry_run,
                                                                   self.silent_mode)
        else:
            total = TrashBinFunctionality.EraseEverythingInBin(self.trash_file_path, self.trash_info_path, self.dry_run,
                                                               self.silent_mode)
        return total

    def Delete(self, file_path):
        size_occupied_by_trash = GetFileSize(self.trash_file_path)
        file_size = GetFileSize(file_path)
        try:
            if self.max_trashcan_size - size_occupied_by_trash > file_size:
                if CheckForAvailableCapacity(self.max_trashcan_capacity, self.trash_info_path):
                    if self.confirmation and not self.silent_mode:
                        if not os.path.exists(file_path):
                            SilentPrint(" File \"{}\" not found".format(file_path),
                                        " File \"{}\" not found;".format(file_path), self.silent_mode)
                            raise OSError(2)
                        question = raw_input(
                            " Do you really want to delete \"{}\"?(Y/N): ".format(os.path.basename(file_path)))
                        if question == 'y' or question == 'Y':
                            TrashBinFunctionality.DeleteFileByPath(file_path, self.trash_file_path,
                                                                   self.trash_info_path,
                                                                   self.path_to_trash, self.dry_run,
                                                                   self.silent_mode)
                    else:
                        TrashBinFunctionality.DeleteFileByPath(file_path, self.trash_file_path, self.trash_info_path,
                                                               self.path_to_trash, self.dry_run,
                                                               self.silent_mode)
                else:

                    if self.confirmation and not self.silent_mode:
                        question = raw_input(" Trashcan is full, delete permanently?(y/n): ")
                        if question == 'y' or question == 'Y':
                            Erase(file_path, self.dry_run)
                    else:
                        Erase(file_path, self.dry_run)
            else:
                if self.confirmation and not self.silent_mode:
                    question = raw_input(" Not enough memory, delete permanently?(Y/N): ")
                    if question == 'y' or question == 'Y':
                        Erase(file_path, self.dry_run)
                else:
                    Erase(file_path, self.dry_run)
                    name = os.path.basename(file_path)
                    SilentPrint(" \"{}\" file has been erased".format(name),
                                " \"{}\" has been erased(too big);".format(name), self.silent_mode)
        except OSError as e:
            if e.args[0] == errno.EACCES:
                SilentPrint(" Denied. \"{}\" is system file or directory".format(os.path.basename(file_path)),
                            " Attempted to delete system file or directory;", self.silent_mode)
            raise

    def DeleteRecursive(self, path, regular, stat):
        logging.debug("- Trying to delete with regex \"{}\":".format(regular))
        return self.DeleteRecursion(path, regular, stat)

    def DeleteRecursion(self, path, regular, stat):
        total = 0
        files_waiting_for_deletion = os.listdir(path)
        for file_name in files_waiting_for_deletion:
            if re.search(regular, file_name):
                total += 1
                try:
                    if self.confirmation and not self.silent_mode:
                        question = raw_input(
                            " Do you really want to delete \"{}\"?(Y/N): ".format(file_name))
                        if question == 'y' or question == 'Y':
                            TrashBinFunctionality.DeleteFileByPath(path + '/' + file_name, self.trash_file_path,
                                                                   self.trash_info_path,
                                                                   self.path_to_trash, self.dry_run, self.silent_mode)
                        else:
                            total -= 1
                    else:
                        TrashBinFunctionality.DeleteFileByPath(path + '/' + file_name, self.trash_file_path,
                                                               self.trash_info_path,
                                                               self.path_to_trash, self.dry_run, self.silent_mode)
                except OSError as e:
                    stat.CountException(e.args[0])
            elif os.path.isdir(path + '/' + file_name):
                total += self.DeleteRecursion(path + '/' + file_name, regular, stat)
        return total

    def Inspect(self):
        return TrashBinFunctionality.InspectBin(self.trash_file_path, self.records_per_page)
########################################################################################################################

    def AutoErase(self):
        logging.debug("- Checking files for auto-erase:")
        file_list = os.listdir(self.trash_info_path)
        for file_name in file_list:
            info_file = open(self.trash_info_path + file_name, 'r')
            line = info_file.read().splitlines()

            timestamp = (datetime.now() - datetime.strptime(line[2], '%Y-%m-%d %H:%M:%S.%f')).total_seconds() * 1000
            if timestamp >= self.time_until_auto_erase:
                file_name = file_name[:-10]
                TrashBinFunctionality.EraseFileFromBin(file_name, self.trash_file_path, self.trash_info_path, self.dry_run, self.silent_mode)
