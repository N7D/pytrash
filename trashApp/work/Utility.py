from os.path import dirname, abspath
import os
import shutil
import time


def GetFileSize(path):
    total_size = 0
    if os.path.isfile(path):
        return os.path.getsize(path)
    for dirpath, dirnames, file_names in os.walk(path):
        for file_name in file_names:
            file_path = os.path.join(dirpath, file_name)
            if os.path.exists(file_path):
                total_size += os.path.getsize(file_path)
    return total_size


def CheckForUniqueness(name, path):
    index = 0
    trash_files = os.listdir(path)
    while True:
        uniqueness_check = True
        for trash_file in trash_files:
            if name == trash_file and index == 0:
                index += 1
                uniqueness_check = False
                break
            elif name + '.' + index.__str__() == trash_file:
                index += 1
                uniqueness_check = False
                break
        if uniqueness_check is True:
            if index > 0:
                return name + '.' + index.__str__()
            else:
                return name


def CheckForAvailableCapacity(max_trashcan_capacity, trash_info_path):
    if not os.path.exists(trash_info_path):
        return False
    trash_info_files = os.listdir(trash_info_path)
    if max_trashcan_capacity > trash_info_files.__len__():
        return True
    else:
        return False


def Move(name, path, trash, destination, dry_run):
    if not os.path.exists(path):
        raise OSError(2)
    elif not os.access(path, os.W_OK):
        raise OSError(13)

    if not dry_run:
        path_to_temp_buffer = dirname(dirname(abspath(__file__)))
        while True:
            if os.path.exists("{}temp/".format(path_to_temp_buffer)):
                time.sleep(0.02)
            else:
                break
        os.mkdir("{}temp/".format(path_to_temp_buffer))
        shutil.move(path, "{}temp".format(path_to_temp_buffer))
        info = CheckForUniqueness(name, destination)
        last_name = os.path.basename(path)
        os.rename("{}temp/{}".format(path_to_temp_buffer, last_name), "{}temp/{}".format(path_to_temp_buffer, info))
        shutil.move("{}temp/{}".format(path_to_temp_buffer, info), destination)
        shutil.rmtree("{}temp".format(path_to_temp_buffer))


def Erase(path, dry_run):
    if not dry_run:
        if os.path.islink(path):
            os.unlink(path)
        elif os.path.isdir(path):
            shutil.rmtree(path)
        else:
            os.remove(path)
    else:
        if not os.access(path, os.W_OK):
            raise OSError(13)
