import logging


def StrToBool(string):
    if type(string) is bool:
        return string
    if string == "True" or string == "true":
        return True
    elif string == "False" or string == "false":
        return False


def SilentPrint(string, log, silent_mode):
    logger = logging.getLogger()
    if not silent_mode and len(string) > 0:
        print string
    logger.debug(log)
