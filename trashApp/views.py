# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import shutil
import time
from os.path import dirname, abspath

from django.shortcuts import render, redirect

import packages
from models import OperationsList, TrashBinsList, DoneOperationsList, DoneTasksList
from multithreaded_proccessing import create_operations_for_regex_delete, launch_check
from views_utility import *
from .forms import NewTrashForms


########################################################################################################################

def trash_bin_list(request):
    error_message = None
    list_of_trash_bins = TrashBinsList.objects.all()

    if "error_message" in request.session:
        if "error_message_seen" in request.session:
            if not request.session["error_message_seen"]:
                error_message = request.session["error_message"]
                request.session["error_message_seen"] = "True"

    for trash_bin in list_of_trash_bins:
        make_trash_bin_object(trash_bin).AutoEraseCheck()

    if request.method == "POST":
        selected_trash_bin = request.POST.get("select")
        path_of_trash_bin_to_delete = request.POST.get("del_trash")
        path_of_edited_trash_bin = request.POST.get("edit_trash")

        if path_of_trash_bin_to_delete is not None:
            for trash_bin in list_of_trash_bins:
                if trash_bin.path_to_trash == path_of_trash_bin_to_delete:
                    trash_bin.delete()
                    if os.path.exists(path_of_trash_bin_to_delete):
                        shutil.rmtree(path_of_trash_bin_to_delete)
            list_of_trash_bins = TrashBinsList.objects.all()
            time.sleep(0.1)

        elif path_of_edited_trash_bin is not None:
            force_overwrite_of_edited = request.POST.get("force_overwrite {}".format(path_of_edited_trash_bin))
            max_trashcan_capacity_of_edited = request.POST.get(
                "max_trashcan_capacity {}".format(path_of_edited_trash_bin))
            max_trashcan_size_of_edited = request.POST.get("max_trashcan_size {}".format(path_of_edited_trash_bin))
            dry_run_of_edited = request.POST.get("dry_run {}".format(path_of_edited_trash_bin))
            erase_by_time_of_edited = request.POST.get("erase_by_time {}".format(path_of_edited_trash_bin))
            time_until_auto_erase_of_edited = request.POST.get(
                "time_until_auto_erase {}".format(path_of_edited_trash_bin))

            for trash_bin in list_of_trash_bins:
                if trash_bin.path_to_trash == path_of_edited_trash_bin:
                    trash_bin.dry_run = check_box_to_bool_converter(dry_run_of_edited)
                    trash_bin.force_overwrite = check_box_to_bool_converter(force_overwrite_of_edited)
                    trash_bin.erase_by_time = check_box_to_bool_converter(erase_by_time_of_edited)
                    if str(max_trashcan_capacity_of_edited).isdigit():
                        trash_bin.max_trashcan_capacity = max_trashcan_capacity_of_edited
                    if str(max_trashcan_size_of_edited).isdigit():
                        trash_bin.max_trashcan_size = max_trashcan_size_of_edited
                    if str(time_until_auto_erase_of_edited).isdigit():
                        trash_bin.time_until_auto_erase = time_until_auto_erase_of_edited
                    trash_bin.save()
                    error_message = "Saved."
        else:
            for trash_bin in list_of_trash_bins:
                if trash_bin.selected:
                    trash_bin.selected = False
                    trash_bin.save()
                if trash_bin.path_to_trash == selected_trash_bin:
                    trash_bin.selected = True
                    trash_bin.save()

    list_is_empty = False
    if len(list_of_trash_bins) == 0:
        list_is_empty = True

    context = {
        "error_message": error_message,
        "list_is_empty": list_is_empty,
        "list_of_trash_bins": list_of_trash_bins,
    }
    return render(request, "trashApp/trash_bin_list.html", context)


########################################################################################################################

def create_new_trash_bin(request):
    error_message = None
    form = NewTrashForms(request.POST or None)
    if request.method == "POST" and form.is_valid():
        form_data = form.cleaned_data
        already_exist = False
        for trash_bin in TrashBinsList.objects.all():
            if trash_bin.path_to_trash == form_data["path_to_trash"]:
                already_exist = True
        if not already_exist and form_data["path_to_trash"] != "":
            new_trash_bin = TrashBinsList(path_to_trash=form_data["path_to_trash"],
                                          dry_run=form_data["dry_run"],
                                          force_overwrite=form_data["force_overwrite"],
                                          erase_by_time=form_data["erase_by_time"],
                                          max_trashcan_capacity=form_data["max_trashcan_capacity"],
                                          max_trashcan_size=form_data["max_trashcan_size"],
                                          time_until_auto_erase=form_data["time_until_auto_erase"])
            try:
                new_trash_bin_object = make_trash_bin_object(new_trash_bin)
                new_trash_bin.save()
                new_trash_bin_object.BuildTrashCan()
                error_message = "Created successfully."
            except OSError as e:
                error_message = get_error_message(e)

            request.session["error_message"] = error_message
            request.session["error_message_seen"] = False
            return redirect("trash_bin_list")
        else:
            error_message = "The path you entered is already occupied by another trash bin."

    context = {
        "error_message": error_message,
        "form": form,
    }
    return render(request, "trashApp/create_new_trash_bin.html", context)


########################################################################################################################

def inspect_trash_bin(request):
    list_of_trash_bins = TrashBinsList.objects.all()
    inspected_entries = None
    error_message = None
    selected_trash_bin = None

    if request.method == "POST":
        selected_trash_bin_object = None
        for trash_bin in list_of_trash_bins:
            if trash_bin.selected:
                selected_trash_bin = trash_bin
                selected_trash_bin_object = make_trash_bin_object(trash_bin)
                break
        if selected_trash_bin_object is None:
            error_message = "You must select trash bin first."
        else:
            request_to_empty = request.POST.get("ClearAll")
            name_of_file_to_be_erased = request.POST.get("delete")
            name_of_file_to_be_recovered = request.POST.get("recovery")

            task_name = generate_task_name()

            if name_of_file_to_be_recovered is not None:
                task = create_new_task(selected_trash_bin, task_name, "Recover")
                new_operation = OperationsList(trash_bin=selected_trash_bin, operation_name="recover",
                                               file_path=name_of_file_to_be_recovered, task=task)
                new_operation.save()

                # error_message = name_of_file_to_be_recovered + " recovered."

            elif name_of_file_to_be_erased is not None:
                task = create_new_task(selected_trash_bin, task_name, "Erase")
                new_operation = OperationsList(trash_bin=selected_trash_bin, operation_name="erase",
                                               file_path=name_of_file_to_be_erased, task=task)
                new_operation.save()

                # error_message = name_of_file_to_be_erased + " erased."

            elif request_to_empty is not None:
                path_to_trash_bin_file = selected_trash_bin_object.trash_file_path
                trash_bin_file_paths = os.listdir(path_to_trash_bin_file)
                if len(trash_bin_file_paths) > 0:
                    task = create_new_task(selected_trash_bin, task_name, "Empty")
                    for file_path in trash_bin_file_paths:
                        new_operation = OperationsList(trash_bin=selected_trash_bin, operation_name="erase",
                                                       file_path=file_path, task=task)
                        new_operation.save()
                        # error_message = "Emptied."

            launch_check()

    time.sleep(0.02)
    trash_bin_object = None
    for trash_bin in list_of_trash_bins:
        if trash_bin.selected:
            trash_bin_object = make_trash_bin_object(trash_bin)
            if not os.path.exists(trash_bin.path_to_trash):
                trash_bin_object.BuildTrashCan()
            break
    if trash_bin_object is None:
        error_message = "You must select trash bin first."
    else:
        file_names = trash_bin_object.Inspect()

        inspected_entries = []
        for file_name in file_names:
            inspected_entries.append(packages.Inspected_Package(file_name, False, False))

        operations = OperationsList.objects.all()
        for operation in operations:
            if operation.trash_bin == selected_trash_bin:
                for inspected_entry in inspected_entries:
                    if operation.file_path == inspected_entry.file_name:
                        if operation.operation_name == "erase":
                            inspected_entry.erased = True
                        elif operation.operation_name == "recover":
                            inspected_entry.recovered = True

    context = {
        "error_message": error_message,
        "inspected_entries": inspected_entries
    }
    return render(request, "trashApp/inspect_trash_bin.html", context)


########################################################################################################################

def history(request):
    error_message = None
    show_details = False

    operations_of_history_task = []
    history_task_entries = []

    if request.method == "POST":
        erase = request.POST.get("erase")
        if erase is not None:
            DoneTasksList.objects.all().delete()
            DoneOperationsList.objects.all().delete()
            error_message = "Erased."

        detailed_task_name = request.POST.get("task")
        if detailed_task_name is not None:
            show_details = True
            done_operation_list = DoneOperationsList.objects.all()
            for done_operation in done_operation_list:
                if done_operation.task_name == detailed_task_name:
                    operations_of_history_task.append(
                        packages.History_Operation_Package(os.path.basename(done_operation.trash_bin_path),
                                                           done_operation.operation_name,
                                                           done_operation.file_path,
                                                           done_operation.status))
            operations_of_history_task = operations_of_history_task[::-1]
        else:
            show_details = False

    done_tasks_list = DoneTasksList.objects.all()
    for done_task in done_tasks_list:
        history_task_entries.append(
            packages.History_Task_Package(os.path.basename(done_task.trash_bin_path), done_task.operation_name,
                                          done_task.task_name, done_task.status))
    history_task_entries = history_task_entries[::-1]

    context = {
        "error_message": error_message,
        "show_details": show_details,
        "history_task_entries": history_task_entries,
        "operations_of_history_task": operations_of_history_task
    }
    return render(request, "trashApp/history.html", context)


########################################################################################################################

def tasks(request):
    error_message = None
    show_details = False
    operations_list = OperationsList.objects.all()
    operations_of_task = []
    task_entries = []

    if request.method == "POST":
        abort = request.POST.get("abort")
        if abort is not None:
            trash_bins = TrashBinsList.objects.all()
            for trash_bin in trash_bins:
                trash_bin.active = False
                trash_bin.save()
            TasksList.objects.all().delete()
            OperationsList.objects.all().delete()
            error_message = "Aborted and deactivated."

        detailed_task_name = request.POST.get("task")
        if detailed_task_name is not None:
            show_details = True

            for operation in operations_list:
                if operation.task.task_name == detailed_task_name:
                    operations_of_task.append(
                        packages.Operation_Package(os.path.basename(operation.trash_bin.path_to_trash),
                                                   operation.operation_name,
                                                   operation.file_path))
            operations_of_task = operations_of_task[::-1]

        else:
            show_details = False

    for task in TasksList.objects.all():
        task_entries.append(
            packages.Task_Package(os.path.basename(task.trash_bin.path_to_trash), task.operation_name, task.task_name))
    task_entries = task_entries[::-1]

    context = {
        "error_message": error_message,
        "show_details": show_details,
        "task_entries": task_entries,
        "operations_of_task": operations_of_task
    }
    return render(request, "trashApp/tasks.html", context)


########################################################################################################################

def help_page(request):
    return render(request, "trashApp/help.html")


########################################################################################################################

def about_page(request):
    return render(request, "trashApp/about.html")


########################################################################################################################

def explorer(request):
    error_message = None
    path_to_user_home = os.path.expanduser('~')
    path_to_explorer_directory = path_to_user_home
    previous_directory = path_to_user_home
    files_to_delete = set()
    list_of_trash_bins = TrashBinsList.objects.all()

    selected_trash_bin = None
    for trash_bin in list_of_trash_bins:
        if trash_bin.selected:
            selected_trash_bin = trash_bin
            break

    if selected_trash_bin is None:
        error_message = "You must select trash bin first."

    if request.method == "POST":
        new_path = request.POST.get("directory")
        file_press = request.POST.get("file")
        path = request.POST.get("delete")
        path_for_regex = request.POST.get("regular_delete")

        if new_path is not None:
            path = new_path

        if file_press:
            path = dirname(abspath(file_press))
        elif path is None or path == "" or path == path_to_user_home:
            previous_directory = path_to_user_home
        else:
            previous_directory = dirname(abspath(path))

        if path is not None:
            current_directory_files = os.listdir(path)
            path_to_explorer_directory = path
        elif path_for_regex is not None and (not os.path.isfile(path_for_regex)):
            current_directory_files = os.listdir(path_for_regex)
            path_to_explorer_directory = path_for_regex
        else:
            current_directory_files = os.listdir(previous_directory)

        if path is not None or path_for_regex is not None:
            for file_name in current_directory_files:
                if path is not None:
                    checkbox = request.POST.get("check {}".format(os.path.join(path, file_name)))
                    if checkbox is not None:
                        files_to_delete.add(os.path.join(path, file_name))
                else:
                    checkbox = request.POST.get("check {}".format(os.path.join(path_for_regex, file_name)))
                    if checkbox is not None:
                        files_to_delete.add(file_name)
            if selected_trash_bin is not None:

                selected_trash_bin_object = make_trash_bin_object(selected_trash_bin)

                task_name = generate_task_name()
                if not os.path.exists(selected_trash_bin_object.path_to_trash):
                    selected_trash_bin_object.BuildTrashCan()
                if path is not None:
                    if len(files_to_delete) > 0:
                        task = create_new_task(selected_trash_bin, task_name, "Delete")
                        for file_path in files_to_delete:
                            new_operation = OperationsList(trash_bin=selected_trash_bin, operation_name="delete",
                                                           file_path=os.path.join(
                                                               path, file_path), task=task)
                            new_operation.save()
                        launch_check()
                        time.sleep(0.3)
                        error_message = "Operation was added to queue."

                elif path_for_regex is not None:
                    path = path_for_regex
                    regex = request.POST.get("regular")
                    if len(files_to_delete) > 0:
                        task = create_new_task(selected_trash_bin, task_name, "Delete")
                        create_operations_for_regex_delete(files_to_delete, path_for_regex, regex, selected_trash_bin,
                                                           task)
                        launch_check()
                        time.sleep(0.3)
                        error_message = "Operation was added to queue."
                    else:
                        error_message = "Select directories first."
            elif path_for_regex is not None:
                path = path_for_regex

        if path is not None:
            current_directory_files = os.listdir(path)
            path_to_explorer_directory = path
        elif path_for_regex is not None and (not os.path.isfile(path_for_regex)):
            current_directory_files = os.listdir(path_for_regex)
            path_to_explorer_directory = path_for_regex
        else:
            current_directory_files = os.listdir(previous_directory)
            path = previous_directory
        current_directories_packages = set()
        current_files_packages = set()

        for current in current_directory_files:
            if os.path.isdir(os.path.join(path, current)):
                current_directories_packages.add(packages.Directory_Package(os.path.join(path, current), current))
            else:
                current_files_packages.add(packages.File_Package(os.path.join(path, current), current))
    else:
        path = path_to_user_home
        current_directory_files = os.listdir(path)
        current_directories_packages = set()
        current_files_packages = set()
        for current in current_directory_files:
            if os.path.isdir(os.path.join(path, current)):
                current_directories_packages.add(packages.Directory_Package(os.path.join(path, current), current))
            else:
                current_files_packages.add(packages.File_Package(os.path.join(path, current), current))
    current_directories_packages = sorted(current_directories_packages, key=lambda x: x.directory_name, reverse=True)
    current_files_packages = sorted(current_files_packages, key=lambda x: x.file_name, reverse=True)

    context = {
        "error_message": error_message,
        "current_directories_packages": current_directories_packages,
        "current_files_packages": current_files_packages,
        "path_to_user_home": path_to_user_home,
        "path_to_explorer_directory": path_to_explorer_directory,
        "path": path,
        "previous_directory": previous_directory
    }
    return render(request, "trashApp/explorer.html", context)
