# -*- coding: utf-8 -*-
"""
Multi-threaded execution module.

It contains functions for background task and operations processing.
"""

from __future__ import unicode_literals

import logging
import re
import threading
import time

from models import OperationsList, DoneOperationsList, TrashBinsList, DoneTasksList
from views_utility import *

task_lock = threading.BoundedSemaphore(value=5)
operation_lock = threading.BoundedSemaphore(value=1)


def create_operations_for_regex_delete(files_to_check, path, regex, current_trash_bin, task):
    """
    Recursively goes through directories and makes operation for every file or directory matching the regex.

    Parameters:
        files_to_check - list of selected for regex deletion directories and files.
        path - current explorer location.
        regex - string with regular expression.
        current_trash_bin - the trash bin to where the operations will belong.
        task - entry of task from database model.
    """
    for file_name in files_to_check:
        if re.search(regex, file_name):

            new_operation = OperationsList(trash_bin=current_trash_bin, operation_name="delete",
                                           file_path=os.path.join(path, file_name), task=task)
            new_operation.save()

        elif os.path.isdir(os.path.join(path, file_name)):
            next_directory_file_list = os.listdir(os.path.join(path, file_name))
            create_operations_for_regex_delete(next_directory_file_list, os.path.join(path, file_name), regex,
                                               current_trash_bin, task)


def launch_check():
    """
    Decides if pre-execution thread launch is needed.
    """
    tasks = TasksList.objects.all()
    trash_bins = TrashBinsList.objects.all()
    activeness = False

    for trash_bin in trash_bins:
        if trash_bin.active:
            activeness = True
            break

    if not activeness:
        new_thread = threading.Thread(target=pre_execution)
        new_thread.start()
    else:
        if len(tasks) == 0:
            for trash_bin in trash_bins:
                if trash_bin.active:
                    trash_bin.active = False
                    trash_bin.save()


def pre_execution():
    """
    Activates trash bin, sets flag and starts thread for every task.
    """
    while True:
        tasks = TasksList.objects.all()

        if len(tasks) == 0:
            break

        for task in tasks:
            if not task.trash_bin.active and not task.being_processed:
                new_thread = threading.Thread(target=task_execution, args=(task,))
                task.trash_bin.active = True
                task.trash_bin.save()
                task.being_processed = True
                task.save()
                new_thread.start()
        time.sleep(0.01)


def task_execution(task):
    """
    Starts thread for each operation of the task.
    Sets flag, waits until operations are finished, sets status and add history entries.

    Parameters:
        task - entry of task from database model.
    """
    time.sleep(0.01)
    task_status = None
    threads_to_wait = []
    operations_of_task = []
    operations_list = OperationsList.objects.all()

    for operation in operations_list:
        if operation.task.task_name == task.task_name:
            operations_of_task.append(operation)

    try:
        task_lock.acquire()
        while True:
            if len(operations_of_task) == 0:
                break
            for operation in operations_of_task:
                if not operation.being_processed:
                    new_thread = threading.Thread(target=operation_execution, args=(operation,))
                    operation.trash_bin.active = True
                    operation.trash_bin.save()
                    operation.being_processed = True
                    operation.save()
                    new_thread.start()
                    threads_to_wait.append(new_thread)
                    operations_of_task.remove(operation)
            time.sleep(0.01)
    except OSError as e:
        task_status = get_error_message(e)
    finally:
        task_lock.release()

    for thread in threads_to_wait:
        thread.join()

    if task_status is None:
        task_status = "OK"
        done_operations_list = DoneOperationsList.objects.all()
        for done_operation in done_operations_list:
            if done_operation.task_name == task.task_name and done_operation.status == "Aborted":
                task_status = "Aborted"
                break
            if done_operation.task_name == task.task_name and done_operation.status != "OK":
                task_status = "FAIL"
                break

    new_done_task = DoneTasksList(trash_bin_path=task.trash_bin.path_to_trash,
                                  operation_name=task.operation_name,
                                  task_name=task.task_name,
                                  status=task_status
                                  )
    new_done_task.save()

    for operation in operations_list:
        if operation.task.task_name == task.task_name:
            operation.delete()

    task.trash_bin.active = False
    task.trash_bin.save()
    task.delete()


def operation_execution(operation):
    """
    Executes the operation. Sets the status and creates history entry when the operation is finished.

    Parameters:
        operation - entry of operation from database model.
    """
    time.sleep(0.01)
    operation_status = None

    trash_bin_object = make_trash_bin_object(operation.trash_bin)

    logger = logging.getLogger()
    try:
        operation_lock.acquire()

        logger.setLevel(logging.DEBUG)
        log = logging.FileHandler(filename=trash_bin_object.trash_log_path, )
        log.setLevel(logging.DEBUG)
        formatter = logging.Formatter('[%(asctime)s] -%(message)s')
        log.setFormatter(formatter)
        logger.addHandler(log)

        trash_bin_active = TrashBinsList.objects.get(pk=operation.trash_bin.pk).active
        if trash_bin_active:
            if operation.operation_name == "delete":
                trash_bin_object.Delete(operation.file_path)
            elif operation.operation_name == "recover":
                trash_bin_object.Recover(operation.file_path)
            elif operation.operation_name == "erase":
                trash_bin_object.Erase(operation.file_path)
            elif operation.operation_name == "empty":
                trash_bin_object.Empty()
            else:
                operation_status = "unhandled operation"
        else:
            operation_status = "Aborted"
    except OSError as e:
        operation_status = get_error_message(e)
    finally:
        logger.removeHandler(log)
        operation_lock.release()

    if operation_status is None:
        operation_status = "OK"

    new_done_operation = DoneOperationsList(trash_bin_path=operation.trash_bin.path_to_trash,
                                            operation_name=operation.operation_name,
                                            file_path=operation.file_path, status=operation_status,
                                            task_name=operation.task.task_name
                                            )
    new_done_operation.save()
