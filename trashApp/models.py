# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models


class TrashBinsList(models.Model):
    path_to_trash = models.CharField(max_length=256)

    dry_run = models.BooleanField(default=False)
    force_overwrite = models.BooleanField(default=False)
    silent_mode = models.BooleanField(default=False)
    erase_by_time = models.BooleanField(default=False)
    max_trashcan_capacity = models.IntegerField(default=15)
    max_trashcan_size = models.IntegerField(default=5000000)
    time_until_auto_erase = models.IntegerField(default=36000)

    active = models.BooleanField(default=False)
    selected = models.BooleanField(default=False)

    def __str__(self):
        return str(self.path_to_trash) + " - " + str(self.selected)


class TasksList(models.Model):
    trash_bin = models.ForeignKey(TrashBinsList, on_delete=models.CASCADE, default=1)
    operation_name = models.CharField(max_length=128, default=None)
    task_name = models.CharField(max_length=256)
    being_processed = models.BooleanField(default=False)

    def __str__(self):
        return str(self.trash_bin.path_to_trash) + " - " + str(self.operation_name) + " - " + str(self.task_name)


class OperationsList(models.Model):
    trash_bin = models.ForeignKey(TrashBinsList, on_delete=models.CASCADE, default=1)
    operation_name = models.CharField(max_length=128, default=None)
    task = models.ForeignKey(TasksList, on_delete=models.CASCADE, default=1)
    file_path = models.CharField(max_length=512, default=None)
    being_processed = models.BooleanField(default=False)

    def __str__(self):
        return str(self.operation_name) + " to " + str(self.trash_bin)


class DoneTasksList(models.Model):
    trash_bin_path = models.CharField(max_length=256, default="None")
    operation_name = models.CharField(max_length=128, default=None)
    task_name = models.CharField(max_length=256)
    status = models.CharField(max_length=256, default=None)

    def __str__(self):
        return str(str(self.operation_name) + " - " + str(self.task_name))


class DoneOperationsList(models.Model):
    trash_bin_path = models.CharField(max_length=256, default="None")
    operation_name = models.CharField(max_length=128)
    file_path = models.CharField(max_length=512, default=None)
    status = models.CharField(max_length=256, default=None)
    task_name = models.CharField(max_length=256, default=None)

    def __str__(self):
        return str(self.operation_name) + " to " + str(self.trash_bin_path)
