class File_Package(object):
    def __init__(self, file_path, file_name):
        self.file_name = file_name
        self.file_path = file_path


class Directory_Package(object):
    def __init__(self, directory_path, directory_name):
        self.directory_name = directory_name
        self.directory_path = directory_path


class Inspected_Package(object):
    def __init__(self, file_name, recovered, erased):
        self.file_name = file_name
        self.recovered = recovered
        self.erased = erased


class Operation_Package(object):
    def __init__(self, trash_ending, operation, path):
        self.trash_ending = trash_ending
        self.operation = operation
        self.path = path


class History_Operation_Package(object):
    def __init__(self, trash_ending, operation, path, status):
        self.trash_ending = trash_ending
        self.operation = operation
        self.path = path
        self.status = status

class Task_Package(object):
    def __init__(self, trash_ending, operation, task_name,):
        self.trash_ending = trash_ending
        self.operation = operation
        self.task_name = task_name


class History_Task_Package(object):
    def __init__(self, trash_ending, operation, task_name, status):
        self.trash_ending = trash_ending
        self.operation = operation
        self.task_name = task_name
        self.status = status
